FROM python:3

ENV PYTHONUNBUFFERED=1
WORKDIR /code

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY ./library .

CMD gunicorn library.wsgi:application --bind 0.0.0.0:$PORT