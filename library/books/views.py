from django.http import HttpResponse
from django.shortcuts import render
from .models import Book


def index(request):
    books = Book.objects.all()

    context = {
        'message': 'Vitej!',
        'footer': 'Follow us on F',
        'books': books
    }
    return render(request, "books/index.html", context)

def detail(request, id):    
    book = Book.objects.filter(id=id).first()

    context = {
        'book': book
    }
    return render(request, "books/detail.html", context)
