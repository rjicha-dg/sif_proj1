# SIF Projekt 1

## Heroku deploy

### Prereqs
- docker
- heroku cli

### Docker build

```bash
$ docker build -t <YOUR_APP_NAME> .
```

### Run locally
```bash
$ docker run -e "PORT=8000" -p 8000:8000 <YOUR_APP_NAME>
```

### Publish to Heroku
```bash
$ docker tag <YOUR_APP_NAME> registry.heroku.com/<YOUR_APP_NAME>/web
$ docker push registry.heroku.com/<YOUR_APP_NAME>/web

$ heroku container:release web -a <YOUR_APP_NAME>
```
